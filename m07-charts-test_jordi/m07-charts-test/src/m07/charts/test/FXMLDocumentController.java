/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07.charts.test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import m07.DAO.Search;
import m07.DAO.DAO;
import m07.DAO.FilmDAO;

/**
 *
 * @author rvallez
 */
public class FXMLDocumentController implements Initializable {

    List<Search> s = new ArrayList();
    Scene scene;
    Stage stage = new Stage();

    String id, tipo;

    @FXML
    private void lineal(ActionEvent event) {
        tipo = "lineal";
    }

    @FXML
    private void barres(ActionEvent event) {
        tipo = "barres";
    }

    @FXML
    private void circular(ActionEvent event) {
        tipo = "circular";
    }

    @FXML
    private void rating(ActionEvent event) {
        id = "rating";
    }

    @FXML
    private void precio(ActionEvent event) {
        id = "precio";
    }

    @FXML
    private void duracion(ActionEvent event) {
        id = "duracion";
    }

    @FXML
    private void buscarAction(ActionEvent event) {
        
        
        switch (id) {
            case "rating":
                rating();
                break;
            case "precio":
                precio();
                break;
            case "duracion":
                duracion();
                break;
            default:

        }
        switch (tipo) {
            case "barres":
                barres(s);
                break;
            case "lineal":
                lineal(s);
                break;
            case "circular":
                circulo(s);

                break;
            default:

        }
        
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void duracion() {

        EntityManagerFactory emf = DAO.getEntityManagerFactory();
        FilmDAO actorDao = new FilmDAO(emf);
        s = actorDao.perDuracio();
        System.out.println(s);
    }

    public void precio() {

        EntityManagerFactory emf = DAO.getEntityManagerFactory();
        FilmDAO actorDao = new FilmDAO(emf);
        s = actorDao.perPrecio();
        System.out.println(s);
    }

    public void rating() {

        EntityManagerFactory emf = DAO.getEntityManagerFactory();
        FilmDAO actorDao = new FilmDAO(emf);
        s = actorDao.perRating();
        System.out.println(s);
    }

    public void lineal(List<Search> resultquery) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart
                = new LineChart<>(xAxis, yAxis);
        XYChart.Series series = new XYChart.Series();
        //defining the axes
        xAxis.setLabel("dias");
        yAxis.setLabel("cantidad");
        //creating the chart
        lineChart.setTitle("pelicules");
        //defining a series
        series.setName("My portfolio");
        resultquery.forEach((film) -> {
            series.getData().add(new XYChart.Data(Double.parseDouble(film.getNumDia()), film.getContador()));
        });
        lineChart.getData().add(series);
        scene = new Scene(lineChart, 800, 600);
    }

    public void circulo(List<Search> resultquery) {
        
        stage.setTitle("pelicules Summary");
        scene = new Scene(new Group());
        stage.setWidth(500);
        stage.setHeight(500);

        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList();
        for (Search film : resultquery) {
            pieChartData.add(new PieChart.Data(film.getNumDia(), film.getContador()));
        }
        final PieChart chart = new PieChart(pieChartData);
        ((Group) scene.getRoot()).getChildren().add(chart);
    }

    public void barres(List<Search> resultquery) {
        
        final CategoryAxis xAxisb = new CategoryAxis();
    final NumberAxis yAxisb = new NumberAxis();
        final BarChart<String, Number> bc
            = new BarChart<>(xAxisb, yAxisb);
        XYChart.Series series = new XYChart.Series();
        xAxisb.setLabel("dias");
        yAxisb.setLabel("cantidad");

        bc.setTitle("pelicules Summary");
        //defining a series
        series.setName("My portfolio");
        for (Search film : resultquery) {

            series.getData().add(new XYChart.Data(String.valueOf(film.getNumDia()), film.getContador()));

        }
        bc.getData().add(series);
        scene = new Scene(bc, 800, 600);

    }

}
