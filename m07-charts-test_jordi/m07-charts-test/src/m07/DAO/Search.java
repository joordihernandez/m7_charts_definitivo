/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m07.DAO;

/**
 *
 * @author jordi_hernandez
 */
public class Search {
    
    long count;
    String value;

    public Search(long contador, String numDia) {
        this.count = contador;
        this.value = numDia;
    }

    public long getContador() {
        return count;
    }

    public void setContador(long contador) {
        this.count = contador;
    }

    public String getNumDia() {
        return value;
    }

    public void setNumDia(String numDia) {
        this.value = numDia;
    }

    @Override
    public String toString() {
        return "auxiliar{" + "contador=" + count + ", numDia=" + value + '}';
    }
    
    
    
}
